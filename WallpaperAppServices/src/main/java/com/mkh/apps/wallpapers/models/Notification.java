package com.mkh.apps.wallpapers.models;

import java.util.Date;
import java.util.Map;

public class Notification extends Model {

    private Long id;
    private String title;
    private String message;
    private String type;
    private String userId;
    private Boolean read = false;
    private Long createDate;
    private Long updateDate;

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }

    public Date getUpdateDate() {
        return new Date(updateDate);
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate.getTime();
    }

    @Override
    public String getModelTag() {
        // TODO Auto-generated method stub
        return "notifications";
    }

    @Override
    public String toJsonString() {
        return super.toJsonString();
    }

    @Override
    public Map<String, Object> toMap() {
        return super.toMap();
    }

    @Override
    public void fromMap(Map<String, Object> map) {
        super.fromMap(map);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
