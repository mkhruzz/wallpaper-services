package com.mkh.apps.wallpapers.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

public class CategoryForm {
	//
	// private static final long serialVersionUID = 6731122189777776263L;

	@NotNull
	@Size(min = 2, max = 100)
	private String name;

	@NotNull
	@Size(min = 2, max = 500)
	private String description;

//	@NotNull
//	private MultipartFile image;
	
	@NotNull
	private MultipartFile file;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

//	public MultipartFile getImage() {
//		return image;
//	}
//
//	public void setImage(MultipartFile image) {
//		this.image = image;
//	}
	
	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile image) {
		this.file = image;
	}

}
