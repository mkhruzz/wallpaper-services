package com.mkh.apps.wallpapers;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mkh.apps.wallpapers.forms.CategoryForm;
import com.mkh.apps.wallpapers.forms.FileForm;
import com.mkh.apps.wallpapers.forms.TagForm;
import com.mkh.apps.wallpapers.models.Category;
import com.mkh.apps.wallpapers.models.File;
import com.mkh.apps.wallpapers.models.Tag;
import com.mkh.apps.wallpapers.storage.StorageFileNotFoundException;
import com.mkh.apps.wallpapers.storage.StorageService;

@Controller
public class FileUploadController {

    private final StorageService storageService;

    @Autowired
    public FileUploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/file")
    public String listFiles(FileForm fileForm, Model model) {
        model.addAttribute("files", storageService.getFiles("0", "name", null));
        model.addAttribute("filesList",
                storageService.loadAll()
                        .map(path -> MvcUriComponentsBuilder
                                .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString())
                                .build().toString())
                        .collect(Collectors.toList()));
        model.addAttribute("tags", storageService.getTags("0", null, null));
        model.addAttribute("categories", storageService.getCategories("0", null, null));
        model.addAttribute("fileForm", fileForm);
        return "fileForm";
    }

    @PostMapping("/file")
    public String listFilesV(@Valid FileForm fileForm, BindingResult bindingResult, Model model,
                             RedirectAttributes redirectAttributes) {
        model.addAttribute("files", storageService.getFiles("0", "name", null));
        model.addAttribute("filesList",
                storageService.loadAll()
                        .map(path -> MvcUriComponentsBuilder
                                .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString())
                                .build().toString())
                        .collect(Collectors.toList()));
        model.addAttribute("tags", storageService.getTags("0", null, null));
        model.addAttribute("categories", storageService.getCategories("0", null, null));
        model.addAttribute("fileForm", fileForm);
        model.addAttribute("bindingResult", bindingResult);
        if (bindingResult.hasErrors()) {
            return "fileForm";
        } else {
            File file = new File();
            file.setName(fileForm.getName());
            file.setDescription(fileForm.getDescription());
            file.setImage(fileForm.getImage());
            file.setImageSmaller(fileForm.getImageSmaller());
            file.setImageSmallest(fileForm.getImageSmallest());
            file.setSource(fileForm.getSource());
            file.setCategoryIds(fileForm.getCategoryIds());
            file.setTagIds(fileForm.getTagIds());
            Date date = new Date();
            file.setCreateDate(date.getTime());
            file.setUpdateDate(date);
            if (file.save()) {
                redirectAttributes.addFlashAttribute("message",
                        "You successfully added a new file: " + fileForm.getName() + "!");
                return "redirect:/file";
            }

        }
        return "fileForm";
    }

    @GetMapping("/tag")
    public String listTags(TagForm tagForm, Model model) {
        model.addAttribute("tags", storageService.getTags("0", "name", null));
        model.addAttribute("tagForm", tagForm);
        return "tagForm";
    }

    @PostMapping("/tag")
    public String listTagsV(@Valid TagForm tagForm, BindingResult bindingResult, Model model,
                            RedirectAttributes redirectAttributes) {
        model.addAttribute("tags", storageService.getTags("0", "name", null));
        model.addAttribute("tagForm", tagForm);
        model.addAttribute("bindingResult", bindingResult);
        if (bindingResult.hasErrors()) {
            return "tagForm";
        } else {
            Tag tag = new Tag();
            tag.setName(tagForm.getName());
            tag.setDescription(tagForm.getDescription());
            Date date = new Date();
            tag.setCreateDate(date.getTime());
            tag.setUpdateDate(date);
            if (tag.save()) {
                redirectAttributes.addFlashAttribute("message",
                        "You successfully added a new tag: " + tagForm.getName() + "!");
                return "redirect:/tag";
            }

        }
        return "tagForm";
    }

    @GetMapping("/category")
    public String listCategories(CategoryForm categoryForm, Model model) {
        model.addAttribute("categories", storageService.getCategories("0", "name", null));
        model.addAttribute("categoryForm", categoryForm);
        return "categoryForm";
    }

    @PostMapping("/category")
    public String listCategoriesV(@Valid CategoryForm categoryForm, BindingResult bindingResult, Model model,
                                  RedirectAttributes redirectAttributes) {
        model.addAttribute("categories", storageService.getCategories("0", "name", null));
        model.addAttribute("categoryForm", categoryForm);
        model.addAttribute("bindingResult", bindingResult);
        if (categoryForm.getFile() != null && categoryForm.getFile().isEmpty()) {
            bindingResult.rejectValue("file", "304", "Must upload a file");
        }
        if (bindingResult.hasErrors()) {
            return "categoryForm";
        } else {
            Category category = new Category();
            category.setName(categoryForm.getName());
            category.setDescription(categoryForm.getDescription());
            storageService.store(categoryForm.getFile());
            category.setImage(categoryForm.getFile().getOriginalFilename());
            Date date = new Date();
            category.setCreateDate(date.getTime());
            category.setUpdateDate(date);
            category.setFiles(new ArrayList<>());
            if (category.save()) {
                redirectAttributes.addFlashAttribute("message",
                        "You successfully added a new category: " + categoryForm.getName() + "!");
                return "redirect:/category";
            }

        }
        return "categoryForm";
    }

    @GetMapping("/")
    public String listUploadedFiles(Model model) {
        model.addAttribute("files",
                storageService.loadAll()
                        .map(path -> MvcUriComponentsBuilder
                                .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString())
                                .build().toString())
                        .collect(Collectors.toList()));

        return "uploadForm";
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {
        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @GetMapping("/admin/dist/img/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveThemeFile(@PathVariable String filename) {
        Resource file = storageService.loadAsResource(filename, Paths.get("AdminLTE/dist/img"));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @GetMapping("/admin/*/*/fonts/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveThemeFonts(@PathVariable String filename) {
        if ("".equals(filename)) {
            System.out.print(filename);
        }
        Resource file = getFile();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @PostMapping("/uploadFile")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        storageService.store(file);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:/";
    }

    @GetMapping("/admin/*")
    @ResponseBody
    public String serveAdmin() {
        return getFiles();
    }

    @GetMapping("/admin")
    @PostMapping("/admin")
    public String serveAdminPage() {
        return "redirect:/admin/index.html";
    }

    public String getFiles() {
        Resource file = getFile();
        try {
            return FileUtils.readFileToString(file.getFile());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    public Resource getFile() {
        HttpServletRequest currentRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();
        String requestURI = currentRequest.getRequestURI();
        // System.out.println(requestURI);
        requestURI = requestURI.replace("/admin/", "");
        return storageService.loadAsResource(requestURI, Paths.get("AdminLTE"));
    }

    @GetMapping("/admin/*/*")
    @ResponseBody
    public String serveAdmin1() {
        return getFiles();
    }

    @GetMapping("/admin/*/*/*")
    @ResponseBody
    public String serveAdmin2() {
        return getFiles();
    }

    @GetMapping("/admin/*/*/*/*")
    @ResponseBody
    public String serveAdmin3() {
        return getFiles();
    }

    @GetMapping("/admin/*/*/*/*/*")
    @ResponseBody
    public String serveAdmin4() {
        return getFiles();
    }

    @GetMapping("/admin/*/*/*/*/*/*")
    @ResponseBody
    public String serveAdmin5() {
        return getFiles();
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        if ("".equals(exc.toString())) {
            System.out.print(exc.toString());
        }
        return ResponseEntity.notFound().build();
    }

}
