package com.mkh.apps.wallpapers;

import com.mkh.apps.wallpapers.models.User;
import com.mkh.apps.wallpapers.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class AppController {

//    private static final String template = "Hello, %s!";
//    private final AtomicLong counter = new AtomicLong();

    private final StorageService storageService;

    @Autowired
    public AppController(StorageService storageService) {
        this.storageService = storageService;
    }

//    @RequestMapping("/greeting")
//    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
//        return new Greeting(counter.incrementAndGet(), String.format(template, name));
//    }

    private static List<Map<String, Object>> getSortedMaps(String sortBy, String direction, List<Map<String, Object>> mapList, StorageService storageService) {
        if (!"".equals(sortBy)) {
            if ("featured".equals(sortBy)) {
                sortBy = "featured";
                direction = "desc";
            } else if ("new".equals(sortBy)) {
                sortBy = "id";
                direction = "desc";
            } else if ("popular".equals(sortBy)) {
                sortBy = "download_count";
                direction = "desc";
            }
            mapList = storageService.sort(mapList, sortBy, direction);
        }
        return mapList;
    }

    @RequestMapping("/categories")
    public List<Map<String, Object>> getCategories(@RequestParam(value = "resolve", defaultValue = "0") String resolve,
                                                   @RequestParam(value = "attribute", defaultValue = "") String attribute,
                                                   @RequestParam(value = "id", defaultValue = "") String id,
                                                   @RequestParam(value = "sort", defaultValue = "") String sortBy,
                                                   @RequestParam(value = "dir", defaultValue = "asc") String direction) {
        List<Map<String, Object>> categories = storageService.getCategories(resolve, attribute.equals("") ? null : attribute,
                id.equals("") ? null : id);
        categories = getSortedMaps(sortBy, direction, categories, storageService);
        return categories;
    }

    @RequestMapping("/featured")
    public List<Map<String, Object>> getFeatured(@RequestParam(value = "resolve", defaultValue = "0") String resolve,
                                                 @RequestParam(value = "attribute", defaultValue = "") String attribute,
                                                 @RequestParam(value = "id", defaultValue = "") String id,
                                                 @RequestParam(value = "sort", defaultValue = "") String sortBy,
                                                 @RequestParam(value = "dir", defaultValue = "asc") String direction) {
        List<Map<String, Object>> files = storageService.getFeatured(resolve, attribute.equals("") ? null : attribute,
                id.equals("") ? null : id);
        files = getSortedMaps(sortBy, direction, files, storageService);
        return files;
    }

    @RequestMapping("/tags")
    public List<Map<String, Object>> getTags(@RequestParam(value = "resolve", defaultValue = "0") String resolve,
                                             @RequestParam(value = "attribute", defaultValue = "") String attribute,
                                             @RequestParam(value = "id", defaultValue = "") String id,
                                             @RequestParam(value = "sort", defaultValue = "") String sortBy,
                                             @RequestParam(value = "dir", defaultValue = "asc") String direction) {
        List<Map<String, Object>> tags = storageService.getTags(resolve, attribute.equals("") ? null : attribute,
                id.equals("") ? null : id);
        tags = getSortedMaps(sortBy, direction, tags, storageService);
        return tags;
    }

    @RequestMapping("/files")
    public List<Map<String, Object>> getFiles(@RequestParam(value = "resolve", defaultValue = "0") String resolve,
                                              @RequestParam(value = "attribute", defaultValue = "") String attribute,
                                              @RequestParam(value = "id", defaultValue = "") String id,
                                              @RequestParam(value = "sort", defaultValue = "") String sortBy,
                                              @RequestParam(value = "dir", defaultValue = "asc") String direction) {
        List<Map<String, Object>> files = storageService.getFiles(resolve, attribute.equals("") ? null : attribute,
                id.equals("") ? null : id);
        files = getSortedMaps(sortBy, direction, files, storageService);
        return files;
    }

    @RequestMapping("/popularFiles")
    public List<Map<String, Object>> getPopularFiles(@RequestParam(value = "resolve", defaultValue = "0") String resolve,
                                                     @RequestParam(value = "attribute", defaultValue = "") String attribute,
                                                     @RequestParam(value = "id", defaultValue = "") String id,
                                                     @RequestParam(value = "sort", defaultValue = "") String sortBy,
                                                     @RequestParam(value = "dir", defaultValue = "asc") String direction) {
        List<Map<String, Object>> files = storageService.getFiles(resolve, attribute.equals("") ? null : attribute,
                id.equals("") ? null : id);
        files = getSortedMaps(sortBy, direction, files, storageService);
        sortBy = "download_count";
        direction = "desc";
        files = storageService.sort(files, sortBy, direction);
        Integer toIndex = 100;
        if (files.size() <= toIndex) {
            toIndex = files.size();
        }
        return files.subList(0, toIndex);
    }

    @RequestMapping("/newFiles")
    public List<Map<String, Object>> getNew(@RequestParam(value = "resolve", defaultValue = "0") String resolve,
                                            @RequestParam(value = "attribute", defaultValue = "") String attribute,
                                            @RequestParam(value = "id", defaultValue = "") String id,
                                            @RequestParam(value = "sort", defaultValue = "") String sortBy,
                                            @RequestParam(value = "dir", defaultValue = "asc") String direction) {
        List<Map<String, Object>> files = storageService.getFiles(resolve, attribute.equals("") ? null : attribute,
                id.equals("") ? null : id);
        files = getSortedMaps(sortBy, direction, files, storageService);
        sortBy = "id";
        direction = "desc";
        files = storageService.sort(files, sortBy, direction);
        Integer toIndex = 100;
        if (files.size() <= toIndex) {
            toIndex = files.size();
        }
        return files.subList(0, toIndex);
    }

    @RequestMapping("/search")
    public List<Map<String, Object>> search(@RequestParam(value = "resolve", defaultValue = "0") String resolve,
                                            @RequestParam(value = "attribute", defaultValue = "") String attribute,
                                            @RequestParam(value = "id", defaultValue = "") String id,
                                            @RequestParam(value = "sort", defaultValue = "") String sortBy,
                                            @RequestParam(value = "dir", defaultValue = "asc") String direction,
                                            @RequestParam(value = "tags", defaultValue = "") String tags,
                                            @RequestParam(value = "categories", defaultValue = "") String categories) {
        List<Map<String, Object>> files = storageService.search(resolve, attribute.equals("") ? null : attribute,
                id.equals("") ? null : id, "".equals(tags) ? null : tags, "".equals(categories) ? null : categories);
        files = getSortedMaps(sortBy, direction, files, storageService);
        return files;
    }

    @RequestMapping("/related")
    public List<Map<String, Object>> related(@RequestParam(value = "id", defaultValue = "") String id,
                                             @RequestParam(value = "limit", defaultValue = "") String limit) {
        String resolve = "0";
        String tags;
        String categories;
        String sortBy = "id";
        String direction = "asc";
        List<Map<String, Object>> filesFirst = storageService.search(resolve, null,
                id.equals("") ? null : id, null, null);
        if (filesFirst.size() > 0) {
            Map<String, Object> file = filesFirst.get(0);
            tags = StringUtils.arrayToCommaDelimitedString(((List) file.get("tagIds")).toArray());
            categories = StringUtils.arrayToCommaDelimitedString(((List) file.get("categoryIds")).toArray());
            resolve = "1";
            List<Map<String, Object>> files = storageService.search(resolve, null,
                    null, "".equals(tags) ? null : tags, "".equals(categories) ? null : categories, id);
            files = getSortedMaps(sortBy, direction, files, storageService);
            Integer toIndex;
            try {
                toIndex = Integer.valueOf(limit);
            } catch (Exception e) {
                toIndex = 6;
            }
            if (files.size() <= toIndex) {
                toIndex = files.size();
            }
            return files.subList(0, toIndex);
        } else {
            return filesFirst;
        }
    }

    @RequestMapping("/users")
    public List<Map<String, Object>> getUsers(@RequestParam(value = "resolve", defaultValue = "0") String resolve,
                                              @RequestParam(value = "attribute", defaultValue = "") String attribute,
                                              @RequestParam(value = "id", defaultValue = "") String id,
                                              @RequestParam(value = "sort", defaultValue = "") String sortBy,
                                              @RequestParam(value = "dir", defaultValue = "asc") String direction,
                                              @RequestParam(value = "deviceId", defaultValue = "") String deviceId) {
        List<Map<String, Object>> users = storageService.getUsers(resolve, "".equals(attribute) ? null : attribute,
                "".equals(id) ? null : id, "".equals(deviceId) ? null : deviceId);
        users = getSortedMaps(sortBy, direction, users, storageService);
        return users;
    }

    @RequestMapping("/notifications")
    public List<Map<String, Object>> getUserNotifications(@RequestParam(value = "resolve", defaultValue = "1") String resolve,
                                                          @RequestParam(value = "attribute", defaultValue = "notifications") String attribute,
                                                          @RequestParam(value = "userId", defaultValue = "") String id,
                                                          @RequestParam(value = "sort", defaultValue = "id") String sortBy,
                                                          @RequestParam(value = "dir", defaultValue = "desc") String direction,
                                                          @RequestParam(value = "deviceId", defaultValue = "") String deviceId) {
        List<Map<String, Object>> users = storageService.getUsers(resolve, "".equals(attribute) ? null : attribute,
                "".equals(id) ? null : id, "".equals(deviceId) ? null : deviceId);
        users = getSortedMaps(sortBy, direction, users, storageService);
        return users;
    }

    @RequestMapping("/add-user")
    public Boolean addUser(@RequestParam(value = "name") String name,
                           @RequestParam(value = "deviceId") String deviceId,
                           @RequestParam(value = "email") String email,
                           @RequestParam(value = "userName") String userName,
                           @RequestParam(value = "password") String password) {
        if (deviceId == null) {
            return false;
        }
        User user = new User();
        user.setDeviceId(deviceId);
        if (name != null) {
            user.setName(name);
        }
        if (email != null) {
            user.setEmail(email);
        }
        if (userName != null){
            user.setUserName(userName);
        }
        if (password != null){
            user.setPassword(password);
        }
        Date date = new Date();
        user.setCreateDate(date.getTime());
        user.setUpdateDate(date);
        return user.save();
    }
}