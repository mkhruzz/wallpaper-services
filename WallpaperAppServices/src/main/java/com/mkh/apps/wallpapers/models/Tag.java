package com.mkh.apps.wallpapers.models;

import java.util.Date;
import java.util.List;

public class Tag extends Model {

	private Long id;
	private String name;
	private String description;
	private List<File> files;
	private Long createDate;
	private Long updateDate;

	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Long createDate) {
		this.createDate = createDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public Date getUpdateDate() {
		return new Date(updateDate);
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate.getTime();
	}

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}
	
	@Override
	public String getModelTag() {
		// TODO Auto-generated method stub
		return "tags";
	}

}
