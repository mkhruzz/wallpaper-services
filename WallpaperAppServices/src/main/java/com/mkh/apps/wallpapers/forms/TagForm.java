package com.mkh.apps.wallpapers.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TagForm {
	//
	// private static final long serialVersionUID = 6731122189777776263L;

	@NotNull
	@Size(min = 2, max = 100)
	private String name;

	@NotNull
	@Size(min = 2, max = 500)
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	

}
