package com.mkh.apps.wallpapers.storage;

import com.mkh.apps.wallpapers.models.Tag;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public interface StorageService {

    void init();

    void store(MultipartFile file);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

    List<Map<String, Object>> getCategories(String resolve, String attribute, String id);

    List<Map<String, Object>> getFeatured(String resolve, String attribute, String id);

    List<Tag> getTags();

    List<Map<String, Object>> search(String resolve, String attribute, String id, String tags, String categories);

    List<Map<String, Object>> getTags(String resolve, String attribute, String id);

    List<Map<String, Object>> getFiles(String resolve, String attribute, String id);

    List<Map<String, Object>> sort(List<Map<String, Object>> list, String sortBy, String direction);

    Resource loadAsResource(String filename, Path path);

    List<Map<String, Object>> search(String resolve, String attribute, String id, String tags, String categories, String except);

    List<Map<String, Object>> getUsers(String resolve, String attribute, String id, String deviceId);
}
