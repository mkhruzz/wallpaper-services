package com.mkh.apps.wallpapers.storage;

import com.jayway.jsonpath.JsonPath;
import com.mkh.apps.wallpapers.models.*;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Stream;

@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    private static void resolveFromJson(String attribute, List<Map<String, Object>> filesList, List<Map<String, Object>> filesAttributeList, List<Map<String, Object>> files, Boolean resolveAll) throws NoSuchFieldException, IllegalAccessException, IOException {
        for (Map<String, Object> map : files) {
            File file = new File();
            file.fromMap(map);
            file.fromMap(file.toMap());
            // if (resolveFiles) {
            // List<Map<String, Object>> files =
            // JsonPath.read(jsonDataString,
            // "$.files[?(" + file.getId() + " in @.fileIds)]");
            // List<File> filesList = new ArrayList<>();
            // for (Map<String, Object> map1 : files) {
            // File file = new File();
            // file.fromMap(map1);
            // filesList.add(file);
            // }
            // file.setFiles(filesList);
            // }
            if (resolveAll) {
                Resource resource = new ClassPathResource("sampleDb.json");
                InputStream resourceInputStream = resource.getInputStream();
                String jsonDataString = IOUtils.toString(resourceInputStream,
                        "utf8");
                List<Map<String, Object>> tags = JsonPath.read(jsonDataString,
                        "$.tags[?(@.id in " + Arrays.toString(file.getTagIds().toArray()) +
                                ")]");
                List<Tag> tagsList = new ArrayList<>();
                for (Map<String, Object> map1 : tags) {
                    Tag tag = new Tag();
                    tag.fromMap(map1);
                    tagsList.add(tag);
                }
                file.setTags(tagsList);
                List<Map<String, Object>> categories = JsonPath.read(jsonDataString,
                        "$.categories[?(@.id in " + Arrays.toString(file.getCategoryIds().toArray()) +
                                ")]");
                List<Category> categoriesList = new ArrayList<>();
                for (Map<String, Object> map1 : categories) {
                    Category category = new Category();
                    category.fromMap(map1);
                    categoriesList.add(category);
                }
                file.setCategories(categoriesList);
            }

            if (attribute != null) {
                HashMap<String, Object> values = new HashMap<>();
                Field idField = File.class.getDeclaredField("id");
                idField.setAccessible(true);
                Object idValue = idField.get(file);
                values.put("id", idValue);
                List<String> attributes = Arrays.asList(attribute.split("\\s*,\\s*"));
                for (String string : attributes) {
                    if (File.class.getDeclaredField(string) != null) {
                        Field field = File.class.getDeclaredField(string);
                        field.setAccessible(true);
                        String attributeValue = (String) field.get(file);
                        values.put(string, attributeValue);
                    }
                }
                filesAttributeList.add(values);
            } else {
                filesList.add(file.toMap());
            }
        }

    }

    private static void addSubFiles(List<File> filesList, List<Map<String, Object>> files) {
        for (Map<String, Object> map1 : files) {
            File file = new File();
            file.fromMap(map1);
            filesList.add(file);
        }
    }

    private Path getRootLocation() {
        return rootLocation;
    }

    @Override
    public void store(MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory " + filename);
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1).filter(path -> !path.equals(this.rootLocation))
                    .map(path -> this.getRootLocation().relativize(path));
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public Resource loadAsResource(String filename, Path path) {
        try {
            if (path == null) {
                path = rootLocation;
            }
            Path file = path.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

    @Override
    public List<Map<String, Object>> getFeatured(String resolve, String attribute, String id) {
        Boolean resolveAll = resolve.equals("1");
        List<Map<String, Object>> filesList = new ArrayList<>();
        List<Map<String, Object>> filesAttributeList = new ArrayList<>();
        try {
            Resource resource = new ClassPathResource("sampleDb.json");
            InputStream resourceInputStream = resource.getInputStream();
            String jsonDataString = IOUtils.toString(resourceInputStream, "utf8");
            List<Map<String, Object>> files;
            Long nowTime = new Date().getTime();
            if (id == null) {
                files = JsonPath.read(jsonDataString,
                        "$.files[?(@.featuredStartDate <=" + nowTime + " && @.featuredStopDate >=" + nowTime + ")]");
            } else {
                files = JsonPath.read(jsonDataString, "$.files[?(@.id ==" + id + " && @.featuredStartDate <=" + nowTime
                        + " && @.featuredStopDate >=" + nowTime + ")]");
            }
            resolveFromJson(attribute, filesList, filesAttributeList, files, resolveAll);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!filesAttributeList.isEmpty()) {
            return filesAttributeList;
        }
        return filesList;
        // Boolean resolveTags = resolve.equals("1");
        // List<File> filesList = new ArrayList<>();
        // try {
        // Resource resource = new ClassPathResource("sampleDb.json");
        // InputStream resourceInputStream = resource.getInputStream();
        // String jsonDataString = IOUtils.toString(resourceInputStream,
        // "utf8");
        // List<Integer> featured = JsonPath.read(jsonDataString,
        // "$.featured[*]");
        // for (Integer id : featured) {
        // List<Map<String, Object>> files = JsonPath.read(jsonDataString,
        // "$.files[?(@.id == " + id + ")]");
        // for (Map<String, Object> map1 : files) {
        // File file = new File();
        // file.fromMap(map1);
        // if (resolveTags) {
        // List<Map<String, Object>> tags = JsonPath.read(jsonDataString,
        // "$.tags[?(@.id in " + Arrays.toString(file.getTagIds().toArray()) +
        // ")]");
        // List<Tag> tagsList = new ArrayList<>();
        // for (Map<String, Object> map : tags) {
        // Tag tag = new Tag();
        // tag.fromMap(map);
        // tagsList.add(tag);
        // }
        // file.setTags(tagsList);
        // }
        // filesList.add(file);
        // }
        // }
        //
        // } catch (Exception e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // return filesList;
    }

    @Override
    public List<Map<String, Object>> getCategories(String resolve, String attribute, String id) {
        Boolean resolveFiles = resolve.equals("1");
        List<Map<String, Object>> categoriesList = new ArrayList<>();
        List<Map<String, Object>> categoriesAttributeList = new ArrayList<>();
        try {
            Resource resource = new ClassPathResource("sampleDb.json");
            InputStream resourceInputStream = resource.getInputStream();
            String jsonDataString = IOUtils.toString(resourceInputStream, "utf8");
            List<Map<String, Object>> categories;
            if (id == null) {
                categories = JsonPath.read(jsonDataString, "$.categories[*]");
            } else {
                categories = JsonPath.read(jsonDataString, "$.categories[?(@.id ==" + id + ")]");
            }
            for (Map<String, Object> map : categories) {
                Category category = new Category();
                category.fromMap(map);
                List<File> filesList = new ArrayList<>();
                if (resolveFiles) {
                    List<Map<String, Object>> files = JsonPath.read(jsonDataString,
                            "$.files[?(" + category.getId() + "in @.categoryIds)]");
                    addSubFiles(filesList, files);
                    category.setFiles(filesList);
                }
                if (attribute != null) {
                    HashMap<String, Object> values = new HashMap<>();
                    Field idField = Category.class.getDeclaredField("id");
                    idField.setAccessible(true);
                    Object idValue = idField.get(category);
                    values.put("id", idValue);
                    List<String> attributes = Arrays.asList(attribute.split("\\s*,\\s*"));
                    for (String string : attributes) {
                        if (File.class.getDeclaredField(string) != null) {
                            Field field = Category.class.getDeclaredField(string);
                            field.setAccessible(true);
                            String attributeValue = (String) field.get(category);
                            values.put(string, attributeValue);
                        }
                    }
                    if (resolveFiles) {
                        values.put("files", filesList);
                    }
                    categoriesAttributeList.add(values);
                } else {
                    categoriesList.add(category.toMap());
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!categoriesAttributeList.isEmpty()) {
            return categoriesAttributeList;
        }
        return categoriesList;
    }

    @Override
    public List<Tag> getTags() {
        List<Tag> tagsList = new ArrayList<>();
        try {
            Resource resource = new ClassPathResource("sampleDb.json");
            InputStream resourceInputStream = resource.getInputStream();
            String jsonDataString = IOUtils.toString(resourceInputStream, "utf8");
            List<Map<String, Object>> tags = JsonPath.read(jsonDataString, "$.tags[*]");
            for (Map<String, Object> map : tags) {
                Tag tag = new Tag();
                tag.fromMap(map);
                tagsList.add(tag);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return tagsList;
    }

    @Override
    public List<Map<String, Object>> getTags(String resolve, String attribute, String id) {
        Boolean resolveFiles = resolve.equals("1");
        List<Map<String, Object>> tagsList = new ArrayList<>();
        List<Map<String, Object>> tagsAttributeList = new ArrayList<>();
        try {
            Resource resource = new ClassPathResource("sampleDb.json");
            InputStream resourceInputStream = resource.getInputStream();
            String jsonDataString = IOUtils.toString(resourceInputStream, "utf8");
            List<Map<String, Object>> tags;
            if (id == null) {
                tags = JsonPath.read(jsonDataString, "$.tags[*]");
            } else {
                tags = JsonPath.read(jsonDataString, "$.tags[?(@.id ==" + id + ")]");
            }
            for (Map<String, Object> map : tags) {
                Tag tag = new Tag();
                tag.fromMap(map);
                List<File> filesList = new ArrayList<>();
                if (resolveFiles) {
                    List<Map<String, Object>> files = JsonPath.read(jsonDataString,
                            "$.files[?(" + tag.getId() + " in @.tagIds)]");
                    addSubFiles(filesList, files);
                    tag.setFiles(filesList);
                }
                if (attribute != null) {
                    HashMap<String, Object> values = new HashMap<>();
                    Field idField = Tag.class.getDeclaredField("id");
                    idField.setAccessible(true);
                    Long idValue = (Long) idField.get(tag);
                    values.put("id", idValue);
                    List<String> attributes = Arrays.asList(attribute.split("\\s*,\\s*"));
                    for (String string : attributes) {
                        if (Tag.class.getDeclaredField(string) != null) {
                            Field field = Tag.class.getDeclaredField(string);
                            field.setAccessible(true);
                            String attributeValue = (String) field.get(tag);
                            values.put(string, attributeValue);
                        }
                    }
                    if (resolveFiles) {
                        values.put("files", filesList);
                    }
                    tagsAttributeList.add(values);

                } else {
                    tagsList.add(tag.toMap());
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!tagsAttributeList.isEmpty()) {
            return tagsAttributeList;
        }
        return tagsList;
    }

    @Override
    public List<Map<String, Object>> search(String resolve, String attribute, String id, String tags, String categories, String except) {
        Boolean resolveAll = resolve.equals("1");
        List<Map<String, Object>> filesList = new ArrayList<>();
        List<Map<String, Object>> filesAttributeList = new ArrayList<>();
        try {
            Resource resource = new ClassPathResource("sampleDb.json");
            InputStream resourceInputStream = resource.getInputStream();
            String jsonDataString = IOUtils.toString(resourceInputStream, "utf8");
            List<Map<String, Object>> files;
            if (id == null) {
                StringBuilder jsonPathBuilder = new StringBuilder();
                jsonPathBuilder.append("$.files[?(");
                String jsonPath;
                if (except != null) {
                    jsonPathBuilder.append("@.id != ").append(except).append(" && ");
                }
                if (categories != null) {
                    List<String> categoriesList = Arrays.asList(categories.split("\\s*,\\s*"));
                    for (String catId : categoriesList) {
                        jsonPathBuilder.append(catId).append(" in @.categoryIds && ");
                    }
                }
                if (tags != null) {
                    List<String> tagsList = Arrays.asList(tags.split("\\s*,\\s*"));
                    for (String tagId : tagsList) {
                        jsonPathBuilder.append(tagId).append(" in @.tagIds && ");
                    }
                }
                if (tags == null && categories == null) {
                    jsonPathBuilder.setLength(0);
                    jsonPathBuilder.append("$.files[*]");
                }
                jsonPath = jsonPathBuilder.toString();
                jsonPath = !jsonPath.contains(" && ") ? jsonPath : jsonPath.substring(0, jsonPath.lastIndexOf(" && "));
                jsonPath += "$.files[*]".equals(jsonPath) ? "" : ")]";
                files = JsonPath.read(jsonDataString, jsonPath);
            } else {
                StringBuilder jsonPathBuilder = new StringBuilder();
                jsonPathBuilder.append("$.files[?(");
                String jsonPath;
                if (except != null) {
                    jsonPathBuilder.append("@.id != ").append(except).append(" && ");
                }
                if (categories != null) {
                    List<String> categoriesList = Arrays.asList(categories.split("\\s*,\\s*"));
                    for (String catId : categoriesList) {
                        jsonPathBuilder.append(catId).append(" in @.categoryIds && ");
                    }
                }
                if (tags != null) {
                    List<String> tagsList = Arrays.asList(tags.split("\\s*,\\s*"));
                    for (String tagId : tagsList) {
                        jsonPathBuilder.append(tagId).append(" in @.tagIds && ");
                    }
                }
                if (tags == null && categories == null) {
                    jsonPathBuilder.setLength(0);
                    jsonPathBuilder.append("$.files[?(@.id == ").append(id);
                }
                jsonPath = jsonPathBuilder.toString();
                jsonPath = !jsonPath.contains(" && ") ? jsonPath : jsonPath.substring(0, jsonPath.lastIndexOf(" && "));
                jsonPath += ")]";
                files = JsonPath.read(jsonDataString, jsonPath);
            }
            resolveFromJson(attribute, filesList, filesAttributeList, files, resolveAll);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!filesAttributeList.isEmpty()) {
            return filesAttributeList;
        }
        return filesList;
//        List<Map<String, Object>> filesList = new ArrayList<>();
//        try {
//            Resource resource = new ClassPathResource("sampleDb.json");
//            InputStream resourceInputStream = resource.getInputStream();
//            String jsonDataString = IOUtils.toString(resourceInputStream, "utf8");
//            List<Map<String, Object>> files = JsonPath.read(jsonDataString, "$.files[*]");
//            for (Map<String, Object> map1 : files) {
//                File file = new File();
//                file.fromMap(map1);
//                List<Map<String, Object>> tags = JsonPath.read(jsonDataString,
//                        "$.tags[?(@.id in " + Arrays.toString(file.getTagIds().toArray()) + ")]");
//
//                List<Tag> tagsList = new ArrayList<>();
//                for (Map<String, Object> map : tags) {
//                    Tag tag = new Tag();
//                    tag.fromMap(map);
//                    tagsList.add(tag);
//                }
//                file.setTags(tagsList);
//                filesList.add(file.toMap());
//            }
//
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        return filesList;
    }

    @Override
    public List<Map<String, Object>> search(String resolve, String attribute, String id, String tags, String categories) {
        return search(resolve, attribute, id, tags, categories, null);
    }

    @Override
    public List<Map<String, Object>> getFiles(String resolve, String attribute, String id) {
        Boolean resolveAll = resolve.equals("1");
        List<Map<String, Object>> filesList = new ArrayList<>();
        List<Map<String, Object>> filesAttributeList = new ArrayList<>();
        try {
            Resource resource = new ClassPathResource("sampleDb.json");
            InputStream resourceInputStream = resource.getInputStream();
            String jsonDataString = IOUtils.toString(resourceInputStream, "utf8");
            List<Map<String, Object>> files;
            if (id == null) {
                files = JsonPath.read(jsonDataString, "$.files[*]");
            } else {
                files = JsonPath.read(jsonDataString, "$.files[?(@.id ==" + id + ")]");
            }
            resolveFromJson(attribute, filesList, filesAttributeList, files, resolveAll);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!filesAttributeList.isEmpty()) {
            return filesAttributeList;
        }
        return filesList;
    }

    public Object sort(List<Map<String, Object>> list, String sortBy) {
        return sort(list, sortBy, "asc");
    }

    public List<Map<String, Object>> sort(List<Map<String, Object>> list, String sortBy, String direction) {
        Comparator<Map<String, Object>> comparator = (final Map<String, Object> o1, final Map<String, Object> o2) -> {
            Object object = o2.get(sortBy);
            Object object2 = o1.get(sortBy);
            if (object == null) {
                object = "";
            }
            if (object2 == null) {
                object2 = "";
            }
            Class cls = String.class;
            Double double1 = 0.0;
            Double double2 = 0.0;
            if (object.getClass().equals(Date.class)) {
                object = ((Date) object).getTime();
                object2 = ((Date) object2).getTime();
            }
            try {
                double1 = Double.parseDouble(object.toString());
                double2 = Double.parseDouble(object2.toString());
                cls = Double.class;
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (Number.class.isAssignableFrom(cls)) {
                Comparator<Double> natural = Comparator.naturalOrder();
                return natural.compare(double2, double1);
            } else {
                return object2.toString().compareTo(object.toString());
            }
        };
        list.sort(comparator);
        if ("desc".equals(direction)) {
            Collections.reverse(list);
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> getUsers(String resolve, String attribute, String id, String deviceId) {
        Boolean resolveAll = resolve.equals("1");
        List<Map<String, Object>> usersList = new ArrayList<>();
        List<Map<String, Object>> usersAttributeList = new ArrayList<>();
        try {
            Resource resource = new ClassPathResource("sampleDb.json");
            InputStream resourceInputStream = resource.getInputStream();
            String jsonDataString = IOUtils.toString(resourceInputStream, "utf8");
            List<Map<String, Object>> users;
            if (id == null && deviceId == null) {
                users = JsonPath.read(jsonDataString, "$.users[*]");
            } else if(deviceId == null) {
                users = JsonPath.read(jsonDataString, "$.users[?(@.id ==" + id + ")]");
            } else {
                users = JsonPath.read(jsonDataString, "$.users[?(@.deviceId ==" + deviceId + ")]");
            }

            resolveUserFromJson(attribute, usersList, usersAttributeList, users, resolveAll);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!usersAttributeList.isEmpty()) {
            return usersAttributeList;
        }
        return usersList;
    }

    private static void resolveUserFromJson(String attribute, List<Map<String, Object>> usersList, List<Map<String, Object>> usersAttributeList, List<Map<String, Object>> users, Boolean resolveAll) throws NoSuchFieldException, IllegalAccessException, IOException {
        for (Map<String, Object> map : users) {
            User user = new User();
            user.fromMap(map);
            user.fromMap(user.toMap());
            if (resolveAll) {
                Resource resource = new ClassPathResource("sampleDb.json");
                InputStream resourceInputStream = resource.getInputStream();
                String jsonDataString = IOUtils.toString(resourceInputStream,
                        "utf8");
                List<Map<String, Object>> downloads = JsonPath.read(jsonDataString,
                        "$.files[?(@.id in " + Arrays.toString(user.getDownloadIds().toArray()) +
                                ")]");
                List<File> fileList = new ArrayList<>();
                for (Map<String, Object> map1 : downloads) {
                    File file = new File();
                    file.fromMap(map1);
                    fileList.add(file);
                }
                user.setDownloads(fileList);
                List<Map<String, Object>> notifications = JsonPath.read(jsonDataString,
                        "$.notifications[?(@.id in " + Arrays.toString(user.getNotificationIds().toArray()) +
                                ")]");
                List<Notification> notificationList = new ArrayList<>();
                for (Map<String, Object> map1 : notifications) {
                    Notification notification = new Notification();
                    notification.fromMap(map1);
                    notificationList.add(notification);
                }
                user.setNotifications(notificationList);
            }

            if (attribute != null) {
                HashMap<String, Object> values = new HashMap<>();
                Field idField = User.class.getDeclaredField("id");
                idField.setAccessible(true);
                Object idValue = idField.get(user);
                values.put("id", idValue);
                List<String> attributes = Arrays.asList(attribute.split("\\s*,\\s*"));
                for (String string : attributes) {
                    if (User.class.getDeclaredField(string) != null) {
                        Field field = User.class.getDeclaredField(string);
                        field.setAccessible(true);
                        String attributeValue = (String) field.get(user);
                        values.put(string, attributeValue);
                    }
                }
                usersAttributeList.add(values);
            } else {
                usersList.add(user.toMap());
            }
        }

    }
}
