package com.mkh.apps.wallpapers.forms;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class FileForm {

	@NotNull
	@Size(min = 2, max = 100)
	private String name;

	@NotNull
	@Size(min = 2, max = 200)
	private String source;

	@NotNull
	@Size(min = 2, max = 500)
	private String description;

	@NotNull
	private String image;

	@NotNull
	private String imageSmaller;

	@NotNull
	private String imageSmallest;

	@NotNull
	private List<Long> categoryIds;

	@NotNull
	private List<Long> tagIds;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImageSmaller() {
		return imageSmaller;
	}

	public void setImageSmaller(String imageSmaller) {
		this.imageSmaller = imageSmaller;
	}

	public String getImageSmallest() {
		return imageSmallest;
	}

	public void setImageSmallest(String imageSmallet) {
		this.imageSmallest = imageSmallet;
	}

	public List<Long> getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(List<Long> categoryIds) {
		this.categoryIds = categoryIds;
	}

	public List<Long> getTagIds() {
		return tagIds;
	}

	public void setTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}

}
