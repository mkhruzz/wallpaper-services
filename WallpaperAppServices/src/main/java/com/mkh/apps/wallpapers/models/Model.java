package com.mkh.apps.wallpapers.models;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public abstract class Model {

    public static Map<String, Object> introspect(Object obj) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        BeanInfo info = Introspector.getBeanInfo(obj.getClass());
        for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
            Method reader = pd.getReadMethod();
            if (reader != null) {
                Object invoke = reader.invoke(obj);
                if (invoke != null)
                    result.put(pd.getName(), invoke);
            }
        }
        return result;
    }

    public void fromJson(String jsonString) {
        try {
            HashMap<String, Object> result = new ObjectMapper().readValue(jsonString, HashMap.class);
            fromMap(result);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public String toJsonString() {
        String jsonString = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setSerializationInclusion(Include.NON_NULL);
            jsonString = objectMapper.writeValueAsString(this);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonString;
    }

    public void fromMap(Map<String, Object> map) {
        try {
            for (Entry<String, Object> entry : map.entrySet()) {
                BeanUtils.setProperty(this, entry.getKey(), entry.getValue());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Map<String, Object> toMap() {
        try {
            return introspect(this);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public Boolean save() {
        try {
            Resource resource = new ClassPathResource("sampleDb.json");
            InputStream resourceInputStream = resource.getInputStream();
            String jsonDataString = IOUtils.toString(resourceInputStream, "utf8");
            String modelTag = getModelTag();
            Double maxId = JsonPath.read(jsonDataString, "$." + modelTag + "[*].id.max()");
            if (maxId == null || maxId < 1) {
                maxId = 0.0;
            }
            this.setId(maxId.longValue() + 1);
            String jsonString = toJsonString();
            ObjectMapper objectMapper = new ObjectMapper();
            HashMap<String, Object> result = objectMapper.readValue(resource.getFile(), HashMap.class);
            ArrayList<HashMap<String, Object>> hashMaps = (ArrayList<HashMap<String, Object>>) result.get(modelTag);
            if (hashMaps == null) {
                hashMaps = new ArrayList<>();
                result.put(modelTag, hashMaps);
            }
            hashMaps.add(objectMapper.readValue(jsonString, HashMap.class));
            objectMapper.writeValue(resource.getFile(), result);
            return true;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    public abstract String getModelTag();

    public abstract void setId(Long id);

}
