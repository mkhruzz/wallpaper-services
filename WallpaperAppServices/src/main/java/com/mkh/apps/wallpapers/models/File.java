package com.mkh.apps.wallpapers.models;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class File extends Model {

    private Long id;
    private String name;
    private String source;
    private String description;
    private String image;
    private String imageSmaller;
    private String imageSmallest;
    private Long download_count;
    private List<Long> categoryIds;
    private List<Long> tagIds;
    private Long createDate;
    private Long updateDate;
    private List<Tag> tags;
    private List<Category> categories;
    private Long featuredStartDate;
    private Long featuredStopDate;
    private Boolean featured;

    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageSmaller() {
        return imageSmaller;
    }

    public void setImageSmaller(String imageSmaller) {
        this.imageSmaller = imageSmaller;
    }

    public String getImageSmallest() {
        return imageSmallest;
    }

    public void setImageSmallest(String imageSmallest) {
        this.imageSmallest = imageSmallest;
    }

    public Long getDownload_count() {
        return download_count;
    }

    public void setDownload_count(Long download_count) {
        this.download_count = download_count;
    }

    public List<Long> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Long> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<Long> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<Long> tagIds) {
        this.tagIds = tagIds;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }

    public Date getUpdateDate() {
        return new Date(updateDate);
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate.getTime();
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public String getModelTag() {
        // TODO Auto-generated method stub
        return "files";
    }

    public Boolean getFeatured() {
        Long nowTime = new Date().getTime();
        if (featuredStartDate != null && featuredStopDate != null) {
            featured = (featuredStartDate <= nowTime) && (nowTime <= featuredStopDate);
        } else {
            featured = false;
        }
        return featured;
    }

    public Date getFeaturedStartDate() {
        return featuredStartDate == null ? null : new Date(featuredStartDate);
    }

    public void setFeaturedStartDate(Date featuredStartDate) {
        this.featuredStartDate = featuredStartDate.getTime();
    }

    public Date getFeaturedStopDate() {
        return featuredStopDate == null ? null : new Date(featuredStopDate);
    }

    public void setFeaturedStopDate(Date featuredStopDate) {
        this.featuredStopDate = featuredStopDate.getTime();
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public String toJsonString() {
        initDefaultValues();
        return super.toJsonString();
    }

    @Override
    public Map<String, Object> toMap() {
        initDefaultValues();
        return super.toMap();
    }

    @Override
    public void fromMap(Map<String, Object> map) {
        super.fromMap(map);
        initDefaultValues();
    }

    public void initDefaultValues() {
        if (this.download_count == null) {
            this.download_count = Long.valueOf(0);
        }
    }
}
