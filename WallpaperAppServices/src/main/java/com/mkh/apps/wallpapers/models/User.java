package com.mkh.apps.wallpapers.models;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class User extends Model {

    private Long id;
    private String name;
    private String deviceId;
    private String email;
    private String userName;
    private String password;
    private List<Long> downloadIds;
    private List<File> downloads;
    private List<Long> notificationIds;
    private List<Notification> notifications;
    private Long createDate;
    private Long updateDate;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<Long> getDownloadIds() {
        return downloadIds;
    }

    public void setDownloadIds(List<Long> downloadIds) {
        this.downloadIds = downloadIds;
    }

    public List<File> getDownloads() {
        return downloads;
    }

    public void setDownloads(List<File> downloads) {
        this.downloads = downloads;
    }

    public List<Long> getNotificationIds() {
        return notificationIds;
    }

    public void setNotificationIds(List<Long> notificationIds) {
        this.notificationIds = notificationIds;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }

    public Date getUpdateDate() {
        return new Date(updateDate);
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate.getTime();
    }

    @Override
    public String getModelTag() {
        // TODO Auto-generated method stub
        return "users";
    }

    @Override
    public String toJsonString() {
        return super.toJsonString();
    }

    @Override
    public Map<String, Object> toMap() {
        return super.toMap();
    }

    @Override
    public void fromMap(Map<String, Object> map) {
        super.fromMap(map);
    }
}
